<section class="contact" id="contact">
		<div class="content">
			<div class="col-1-3">
			<% loop $ContactInfo %>
				<h2 class="text-light">$Title:</h2>
				<% if $PhoneNumber %>
					<p class="title-03 color-hot">Phone:</p>
					<p class="text-light">$PhoneNumber</p>
				<% end_if %>
				<% if $StreetAddress %>
				<p class="title-03 color-hot">Address:</p>
				<div class="vcard address">
					<p class="fn n address__line text-light">
						<span class="given-name"></span>
						<span class="additional-name"></span>
					 	<span class="family-name"></span>
					</p>
					<div class="adr">
						<p class="street-address address__line text-light">$StreetAddress</p>
						<p class="address__line text-light">
							<span class="locality">$Locality</span>
							, 
							<span class="region">$Region</span>
							, 
							<span class="postal-code">$PostalCode</span>
						</p>
					</div>
				</div>
				<% end_if %>
				<% if $MapLink %>
				<a target="_blank" href="$MapLink" class="btn btn--primary btn--sm">View on Google Maps</a>
				<% end_if %>
				<% end_loop %>
			</div>
			<div class="col-2-3">
				<blockquote class="title-03 zeta color-platinum">$SiteConfig.FooterText</p>
			</div>
		</div>
	</section>
	
	<footer>
		<div class="content">

		</div>
	</footer>