<?php
class Page extends SiteTree {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

	public function ContactInfo() {
		$ContactInfo = Contact::get()->First();
		return $ContactInfo;
	}

	public function HelloBarSelector() {
		$Selector = HomePage::get()->First() ;
		$Selector = $Selector->HelloBarSelect;
		return $Selector;
	}

	public function ShowHelloBar($itemID = 1) {
		$HelloBars = HelloBar::get()->byID($itemID);
		$HelloBars = $HelloBars->HelloBarText;
		return $HelloBars;
	}

	public function PlanFeatureList($ID) {
		$PlanFeatureIDList = ServicePlan::get()->ByID($ID)->PlanFeatures;
		$PlanFeatureIDList = explode(',',$PlanFeatureIDList);
		$ArrayList = new ArrayList($PlanFeatureIDList);
		$list = new ArrayList();
		for($i=0;$i<$ArrayList->count();$i++) {
			$CurrentPlanFeature = Pricing::get()->First()->PlanFeatures()->byID($ArrayList[$i])->PlanFeatureText;
			$do = new DataObject();
			$do->Feature = $CurrentPlanFeature;
			$list->push($do);
		}
		//debug::dump($list);
		return $list;
	}

	public function ArrayTest() {
		$_list = array('one', 'two', 'three');
		$ArrayList = new ArrayList($_list);

		$list = new ArrayList();
		foreach($_list as $item) {
			$do = new DataObject();
			$do->TestItem = $item;
			$list->push($do);
		}
		return $list;
	}

}
