<?php

class Test extends Page {
	private static $db = array (
		'Test' => 'Text',
	);
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main', TextField::create('Test','Test content'),'Content');

	    return $fields;
	}
}

class Test_Controller extends Page_Controller {
	
}