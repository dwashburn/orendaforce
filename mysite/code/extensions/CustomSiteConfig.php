<?php

class CustomSiteConfig extends DataExtension {

    private static $db = array(
        //'FooterContent' => 'HTMLText'
        'HeaderTextFirstWord' => 'Varchar',
        'HeaderText' => 'Text',
        'FooterText' => 'HTMLText',
        'GAVerification' => 'HTMLText',
        'GAScript' => 'HTMLText'
    );

    public function updateCMSFields(FieldList $fields) {
        // $fields->addFieldToTab("Root.Main", 
        //     new HTMLEditorField("FooterContent", "Footer Content")
        // );
        $fields->addFieldToTab("Root.SiteOptions",
        	TextField::create('HeaderTextFirstWord', 'Header Text (First Word)')
        );
        $fields->addFieldToTab("Root.SiteOptions",
        	TextareaField::create('HeaderText', 'Header Text (minus first word)')
        );
        $fields->addFieldToTab("Root.SiteOptions",
        	TextareaField::create('FooterText', 'Footer Text (sitewide)')
        );
        $fields->addFieldToTab("Root.Analytics",
        	TextareaField::create('GAVerification', 'Google Analytics Site Verification')
        );
        $fields->addFieldToTab("Root.Analytics",
        	TextareaField::create('GAScript', 'Google Analytics Script')
        );

        return $fields;

    }
}
