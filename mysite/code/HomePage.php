<?php

class HomePage extends Page {
	private static $db = array (
		'HelloBarSelect' => 'Varchar',
	);

	private static $many_many = array(
        'HelloBars' => 'HelloBar'
    );

	public function getCMSFields(){
		$fields = parent::getCMSFields();
		$fields->addFieldToTab('Root.HelloBar', GridField::create(
			'HelloBars',
			'Hello Bar Text', 
			$this->HelloBars(),
			GridFieldConfig_RecordEditor::create()
		));
		$fields->addFieldToTab('Root.HelloBar', DropdownField::create(
				'HelloBarSelect',
				'Choose Your Hello Bar (this will be sitewide)',
				HelloBar::get()->map('ID', 'HelloBarText')
			)
			->setEmptyString('(none)'));
		return $fields;
	}
	
}

class HomePage_Controller extends Page_Controller {

	public function ServiceTeasers($count = 2) {
	    $holder = ServicesHolder::get()->First();
	    return ($holder) ? ServicesPage::get()->limit($count) : false;
	}

	public function MembershipPlans() {
	    $Pricing = Pricing::get()->First();
	    return $Pricing;
	}

}