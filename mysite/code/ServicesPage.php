<?php

class ServicesPage extends Page {
	private static $db = array (
		'ServiceName' => 'Varchar',
		'TeaserTitle' => 'Varchar',
		'Teaser' => 'HTMLText',
	);
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main', TextField::create('ServiceName','Name of Service'),'Content');
		$fields->addFieldToTab('Root.Main', TextField::create('TeaserTitle','Teaser Title'),'Content');
		$fields->addFieldToTab('Root.Main', HtmlEditorField::create('Teaser'),'Content');

	    return $fields;
	}
}

class ServicesPage_Controller extends Page_Controller {
	
}