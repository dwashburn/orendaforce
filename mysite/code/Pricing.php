<?php
class Pricing extends Page {
	private static $many_many = array (
		'ServicePlans' => 'ServicePlan',
		'PlanFeatures' => 'PlanFeature',
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldsToTab('Root.Plans', GridField::create(
			'ServicePlans',
			'Orenda Force Service Plan Information',
			$this->ServicePlans(),
			GridFieldConfig_RecordEditor::create()
		));
		$fields->addFieldsToTab('Root.Plans', GridField::create(
			'PlanFeatures',
			'Manage Plan Features',
			$this->PlanFeatures(),
			GridFieldConfig_RecordEditor::create()
		));

		return $fields;
	}
}

class Pricing_Controller extends Page_Controller {

}