<?php

	class HelloBar extends DataObject {
		private static $db = array (
			'HelloBarText' => 'HTMLText',
		);

		private static $belongs_many_many = array (
			'HelloBar' => 'HomePage',
		);
		private static $summary_fields = array (
			'ID' => 'ID',
			'HelloBarText' => 'Hello Bar Text',
		);

		public function getCMSFields() {
			$fields = FieldList::create(
				TextField::create('HelloBarText')
			);
			return $fields;
		}
	}