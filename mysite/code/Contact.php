<?php

class Contact extends Page {
	private static $db = array (
		'PhoneNumber' => 'Varchar',
		'StreetAddress' => 'Varchar',
		'Locality' => 'Varchar',
		'Region' => 'Varchar(2)',
		'PostalCode' => 'Varchar',
		'MapLink' => 'Text'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main', 
			TextField::create('PhoneNumber', 'Phone Number'), 'Content');
		$fields->addFieldToTab('Root.Main', 
			TextField::create('StreetAddress', 'Street Address'), 'Content');
		$fields->addFieldToTab('Root.Main', 
			TextField::create('Locality', 'City'), 'Content');
		$fields->addFieldToTab('Root.Main', 
			TextField::create('Region', 'State'), 'Content');
		$fields->addFieldToTab('Root.Main', 
			TextField::create('PostalCode', 'Zip Code'), 'Content');
		$fields->addFieldToTab('Root.Main', 
			TextField::create('MapLink', 'Map Link'), 'Content');
		return $fields;
	}
}
class Contact_Controller extends Page_Controller {

}