<?php

	class PlanFeature extends DataObject {
		private static $db = array (
			'PlanFeatureText' => 'Varchar',
		);

		private static $belongs_many_many = array (
			'Prices' => 'Pricing',
		);

		private static $summary_fields = array (
			'ID' => 'ID',
			'PlanFeatureText' => 'Plan Feature',
		);

		public function getCMSFields() {
			$fields = FieldList::create(
				TextField::create('PlanFeatureText')
			);
			return $fields;
		}
	}