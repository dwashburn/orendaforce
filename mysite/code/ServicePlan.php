<?php

	class ServicePlan extends DataObject {

	private static $db = array (
		'PlanColor' => 'Varchar',
		'PlanName' => 'Varchar',
		'PlanPrice' => 'Varchar',
		'PlanRenewal' => 'Varchar',
		'PlanFeatures' => 'Text',
	);

	private static $belongs_many_many = array (
		'Prices' => 'Pricing',
	);

	private static $summary_fields = array (
		'ID' => 'ID',
		'PlanName' => 'Name of Plan',
		'PlanPrice' => 'Price',
		'PlanRenewal' => 'Renewal Period',
	);
	public function getCMSFields() {
		$fields = FieldList::create(
			TextField::create('PlanName', 'Plan Name'),
			TextField::create('PlanPrice', 'Plan Price'),
			DropdownField::create(
				'PlanRenewal',
				'Plan renewal period',
				array(
					'/mo.' => 'Monthly',
					'/yr.' => 'Annual'
				)
			)
			->setEmptyString('(none)'),
			CheckboxSetField::create(
				'PlanFeatures',
				'Choose Your Plan Features',
				PlanFeature::get()->map('ID', 'PlanFeatureText')
			),
			DropdownField::create(
				'PlanColor',
				'Color of plan element',
				array(
					'primary' => 'Primary (gold)',
					'secondary' => 'Secondary (blue)',
					'other'=> 'Other (grey)'
				)
			)
			->setEmptyString('(Please choose a color)')
		);
		return $fields;
	}

	public function getServicePlans() {
		return $this->Pricing();
	}
}